package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import main.NewtonGUI.State;

import org.apache.commons.math3.util.ArithmeticUtils;
import org.junit.Test;

public class DrawPanel extends Panel {
	private static final int dragTolerance = 20;
	private static final int controlPointSize = 10;
	private static int k = 0;

	private List<Point> controlPoints = new ArrayList<>();
	private List<Point> curvePoints = new ArrayList<>();
	private Rectangle selectionFrame = new Rectangle(20,20);
	private boolean drawSelectionFrame = false;
	private Point2D draggedPoint = null;
	
	public DrawPanel() {
		super();
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(Color.BLACK);
		g.clearRect(0, 0, this.getWidth(), this.getHeight());
		g.drawString(controlPoints.size() + " / 20", 5, 20);
		
		for (Point2D point : controlPoints) {
			g.fillArc((int) point.getX()-controlPointSize/2, (int) point.getY()-controlPointSize/2, controlPointSize, controlPointSize, 0, 360);
			g.drawString((int) point.getX() + " | " + (int) point.getY(), (int) point.getX()+10, (int) point.getY()+5);
		}
		for (Point2D point : curvePoints) {
			g.fillArc((int) point.getX(), (int) point.getY(), 1, 1, 0, 360);
		}

		g.setColor(Color.RED);
		if (drawSelectionFrame) {
			g.drawRect(selectionFrame.x-selectionFrame.width/2, selectionFrame.y-selectionFrame.height/2, selectionFrame.width, selectionFrame.height);
		}
		g.setColor(Color.RED);
		drawCoordinateSystem(g);
	}

	private void drawCoordinateSystem(Graphics g) {
		g.drawLine(5, 5, this.getWidth(), 5);
		g.drawLine(5, 5, 5, this.getHeight());
		
		int currentPos = 0;
		while(currentPos < this.getWidth()) {
			g.drawLine(currentPos, 5, currentPos, 10);
			g.drawString("" + currentPos, currentPos, 25);
			g.drawLine(5, currentPos, 10, currentPos);
			g.drawString("" + currentPos, 25, currentPos);
			currentPos += 100;
		}
	}

	public void addPoint(Point point) {
		if(controlPoints.size() >= 20) {
			System.out.println("No more points to prevent arithmetic exceptions...");
			return;
		}
		
		controlPoints.add(point);
		calcCurvePoints();
		this.repaint();
	}
	
	public Point2D getSelectedPoint(Point mousePosition) {
		for (Point2D point : controlPoints) {
			if (pointInSelection(point, mousePosition)) {
				return point;
			}
		}
		return null;
	}
	
	public boolean pointInSelection(Point2D point, Point mousePosition) {
		if(epsilonEqual(point.getX(), mousePosition.getX(), dragTolerance) && epsilonEqual(point.getY(), mousePosition.getY(), dragTolerance)) {
			return true;
		}
		return false;
	}

	public static boolean epsilonEqual(double x, double reference, int tolerance) {
		if (x >= reference-tolerance && x<= reference+tolerance) {
			return true;
		}
		return false;
	}
	
	public void setSelectionFramePosition(Point2D selection) {
		setSelectionFramePosition((int)selection.getX(), (int)selection.getY());
	}

	public void setSelectionFramePosition(int x, int y) {
		selectionFrame.x = x;
		selectionFrame.y = y;
	}

	// The first for-loop iterates over the interval k [0-1] in which the
	// curve is defined.
	public void calcCurvePoints() {
		curvePoints = new ArrayList<>();
		k = controlPoints.size() - 1;

		for (float width = 0; width <= 1000; width += 1) {
			float t = width;
			Point newPoint = new Point();
			

			newPoint.y = (int) Newton(t);
			newPoint.x = (int) width;
			
			curvePoints.add(newPoint);
		}
		System.out.println(curvePoints);
	}
	
	private float Newton(float t) {
		float result = 0.0f;
		
		for(int j = 0; j < k; j++) {
			
			float a = a(j);
			
			float n = n(j, t);
			
			result += a * n;
		}
		
		return result;
	}

	private float n(int j, float t) {
		float n = 1.0f;
		for (int i = 0; i < j; i++ ) {

			n *= (t - controlPoints.get(i).getX());
		}
		return n;
	}
	
	private float a(int end) {
		if (end == 0) {
			return (float) controlPoints.get(0).getY();
		}
		float result = 0.0f;
		
		for(int i = 0; i < end; i++) {
			result += devidedDifference(controlPoints.subList(0, end));
		}
		return result;
	}
	
	private float devidedDifference(List<Point> points) {
		float result = 0.0f;
		
		if (points.size() > 1) {
			float temp =  (float) ((devidedDifference(points.subList(1, points.size())) - devidedDifference(points.subList(0, points.size()-1))));
			temp /= (points.get(points.size()-1).getX() - points.get(0).getX());
			result = temp;
		} else if (points.size() == 1) {
			return points.get(0).y;
		}
		return result;
	}
	
	@Test
	public void newtonValues() {
		this.controlPoints = new ArrayList<>();
		Point p0 = new Point(10, 10);
		Point p1 = new Point(15, 15);
		Point p2 = new Point(20, 20);
		Point p3 = new Point(25, 15);
		controlPoints.add(p0);
		controlPoints.add(p1);
		controlPoints.add(p2);
		controlPoints.add(p3);

		this.k = controlPoints.size() - 1;

		Assert.assertEquals(10, Newton(10), 1);
		Assert.assertEquals(15, Newton(15), 1);
		this.controlPoints = new ArrayList<>();
		this.k = 0;
	}

	@Test
	public void devidedDifferencesValues() {
		List<Point> points = new ArrayList<>();
		
		Point p0 = new Point(10, 10);
		Point p1 = new Point(15, 15);
		Point p2 = new Point(20, 20);
		Point p3 = new Point(20, 20);
		points.add(p0);
		Assert.assertEquals(p0.getY(), devidedDifference(points), 0.01f);
		points.add(p1);
		Assert.assertEquals(1, devidedDifference(points), 0.01f);
		points.add(p2);
		Assert.assertEquals(0, devidedDifference(points), 0.01f);
	}
	
	private boolean between(float value, float lower, float upper) {
		return (value >= lower && value < upper);
	}
	
	@Test
	public void betweenValues() {
		Assert.assertTrue(between(2.01f, 2, 3));
		Assert.assertTrue(between(2, 2, 3));
		Assert.assertFalse(between(1, 2, 3));
		Assert.assertFalse(between(3, 2, 3));
	}

	public Point2D getDraggedPoint() {
		return draggedPoint;
	}

	public void setDraggedPoint(Point2D draggedPoint) {
		this.draggedPoint = draggedPoint;
	}

	public void clearControlPoints() {
		controlPoints = new ArrayList<>();
		curvePoints = new ArrayList<>();
		repaint();
		
	}

	public boolean isDrawSelectionFrame() {
		return drawSelectionFrame;
	}

	public void setDrawSelectionFrame(boolean drawSelectionFrame) {
		this.drawSelectionFrame = drawSelectionFrame;
	}
	
}
